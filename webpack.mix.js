let mix = require('laravel-mix');

mix.setPublicPath("./");
mix.sass('assets/scss/acf-gallery_meta-input.scss', 'assets/css/')
   .js('assets/js/acf-gallery_meta-input.js', 'assets/js/acf-gallery_meta-input.min.js')
   .options({processCssUrls: false});
