(function($) {
    if (typeof(acf) == 'undefined') return;

    acf.fields.gallery.edit_attachment = function(id) {
        var self = this,
            $field = this.$field;

        var frame = acf.media.popup({
            mode:       'edit',
            title:      acf._e('image', 'edit'),
            button:     acf._e('image', 'update'),
            attachment: id,
            select:     function( attachment ){
                self.set('$field', $field).render_attachment( attachment );
                self.fetch( id );
            }
        });

        // custom code
        frame.on('open', function() {
            var $sidebar = $('.media-sidebar', frame.$el);
            var $compat = $('.compat-item', frame.$el);

            var ajaxdata = {
                action:     'acf/fields/gallery/get_attachment_meta',
                field_key   : self.$field.data('key'),
                id          : id
            };

            $.ajax({
                url:        acf.get('ajaxurl'),
                data:       acf.prepare_for_ajax(ajaxdata),
                dataType:   'html',
                type:       'post',
                success: function( html ){
                    if (! html) return;

                    $compat.prepend(html);
                    acf.do_action('append', $compat);
                }
            });
        });
    };
})(jQuery);
