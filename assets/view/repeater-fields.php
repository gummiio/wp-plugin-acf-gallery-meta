<tr class="acf-field acf-field-setting-sub_fields" data-setting="repeater" data-name="sub_fields">
    <td class="acf-label">
        <label><?php _e('Media Fields', 'acf-gallery_meta'); ?></label>
        <p class="description">
            <?php _e('Fields for each medias in the gallery', 'acf-gallery_meta'); ?>
        </p>
    </td>

    <td class="acf-input">
        <?php
            acf_get_view('field-group-fields', [
                'fields' => $field['sub_fields'],
                'parent' => $field['ID']
            ]);
        ?>
    </td>
</tr>
