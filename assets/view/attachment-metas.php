<div class="postbox acf-postbox seamless">
    <div class="inside acf-fields -top">
        <?php echo $this->renderMediaMetaFields($options, $field, 'div'); ?>
    </div>
</div>

<style type="text/css">
<?php foreach ($field['hidden_media_fields'] as $name): ?>
    .acf-media-modal.-edit label.setting[data-setting="<?php echo $name; ?>"] { display: none !important; }
<?php endforeach; ?>
</style>
