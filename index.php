<?php
/*
    Plugin Name: Advanced Custom Fields: Gallery Meta
    Plugin URI: https://acf-gallery-meta.gummi.io/
    Description: Extends Acf's gallery field. Allow custom fields to be set for the medias that's uploaded to the gallery.
    Version: 1.0.0
    Author: Alan Chen
    License: GPLv2 or later
    License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No script kiddies please!');

require_once dirname(__FILE__) . '/vendor/autoload.php';

global $acfGalleryMeta;
$acfGalleryMeta = new \GummiIO\AcfGalleryMeta\GalleryMeta(__FILE__, '1.0.0');
