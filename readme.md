# Advanced Custom Fields Addon - Gallery Meta

[ ![Bitbucket Pipeline Status for gummiio/wp-plugin-acf-gallery-meta](https://bitbucket-badges.atlassian.io/badge/gummiio/wp-plugin-acf-gallery-meta.svg)](https://bitbucket.org/gummiio/wp-plugin-acf-gallery-meta/overview)

Extends Acf's gallery field. Allow custom fields to be set for the medias that's uploaded to the gallery.


#### - Example of the gallery field setting looks like, with additional sub fields
![Screenshot 1](screenshots/1.jpg)

#### - The additional sub fields will then rendered when editing the media via gallery's panel
![Screenshot 2](screenshots/2.jpg)
