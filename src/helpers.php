<?php

function acf_gallery_meta($library = null) {
    global $acfGalleryMeta;

    if (! $library) {
        return $acfGalleryMeta;
    }

    return isset($acfGalleryMeta->$library)? $acfGalleryMeta->$library : null;
}
