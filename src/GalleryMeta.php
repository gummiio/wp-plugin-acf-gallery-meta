<?php

namespace GummiIO\AcfGalleryMeta;

use GummiIO\AcfGalleryMeta\AcfGallery\Ajax;
use GummiIO\AcfGalleryMeta\AcfGallery\Field;
use GummiIO\AcfGalleryMeta\AcfGallery\FieldGroup;
use GummiIO\AcfGalleryMeta\AcfGallery\Script;
use GummiIO\AcfGalleryMeta\Core\Updater;

class GalleryMeta
{
    protected $file;
    protected $version;

    public function __construct($file, $version)
    {
        $this->file = $file;
        $this->version = $version;

        $this->initialize();
    }

    public function initialize()
    {
        $this->updater = new Updater;

        add_action('acf/init', [$this, 'loadGalleryExtends']);
        add_action('plugins_loaded', [$this, 'loadLanguages']);
        add_action('admin_notices', [$this, 'printAcfRequiredNotice']);

        return $this;
    }

    public function loadGalleryExtends()
    {
        $this->galleryScript     = new Script;
        $this->galleryField      = new Field;
        $this->galleryAjax       = new Ajax;
    }

    public function loadLanguages()
    {
        load_plugin_textdomain('acf-gallery_meta', false, $this->path('lang'));
    }

    public function printAcfRequiredNotice()
    {
        if (class_exists('acf')) {
            return;
        }

        printf('
            <div class="notice notice-error">
                <p>%s</p>
            </div>',
            sprintf(
                __('%s requires the plugin %s to be activated.', 'acf-gallery_meta'),
                '<b>' . __('ACF Gallery Meta', 'acf-gallery_meta') . '</b>',
                '<b>' . __('Advanced Custom Fields Pro', 'acf-gallery_meta') . '</b>'
            )
        );
    }

    public function version()
    {
        return $this->version;
    }

    public function file()
    {
        return $this->file;
    }

    public function path($path = '')
    {
        return untrailingslashit(plugin_dir_path($this->file) . untrailingslashit($path));
    }

    public function url($uri = '')
    {
        return untrailingslashit(plugin_dir_url($this->file) . untrailingslashit($uri));
    }
}
