<?php

namespace GummiIO\AcfGalleryMeta\AcfGallery;

class Script
{
    public function __construct()
    {
        add_action('acf/input/admin_enqueue_scripts', [$this, 'enqueyeInputScripts']);
    }

    public function enqueyeInputScripts()
    {
        $min = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG)? '.min' : '';

        $jsFile = "assets/js/acf-gallery_meta-input{$min}.js";
        $cssFile = 'assets/css/acf-gallery_meta-input.css';

        wp_enqueue_script(
            'acf-gallery_meta-input',
            acf_gallery_meta()->url($jsFile),
            ['acf-pro-input'],
            filemtime(acf_gallery_meta()->path($jsFile))
        );

        wp_enqueue_style(
            'acf-gallery_meta-input',
            acf_gallery_meta()->url($cssFile),
            ['acf-pro-input'],
            filemtime(acf_gallery_meta()->path($cssFile))
        );
    }
}
