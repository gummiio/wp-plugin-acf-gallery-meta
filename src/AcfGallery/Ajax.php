<?php

namespace GummiIO\AcfGalleryMeta\AcfGallery;

class Ajax
{
    public function __construct()
    {
        $this->galleryField = acf_get_field_type('gallery');

        // unhook the original one
        remove_action('wp_ajax_acf/fields/gallery/get_attachment', [$this->galleryField, 'ajax_get_attachment']);
        remove_action('wp_ajax_nopriv_acf/fields/gallery/get_attachment', [$this->galleryField, 'ajax_get_attachment']);

        // add ours
        add_action('wp_ajax_acf/fields/gallery/get_attachment', [$this, 'ajaxGetAttachment']);
        add_action('wp_ajax_nopriv_acf/fields/gallery/get_attachment', [$this, 'ajaxGetAttachment']);

        // for media popup
        add_action('wp_ajax_acf/fields/gallery/get_attachment_meta', [$this, 'ajaxGetAttachmentMeta']);
        add_action('wp_ajax_nopriv_acf/fields/gallery/get_attachment_meta', [$this, 'ajaxGetAttachmentMeta']);

        add_action('wp_ajax_save-attachment-compat', [$this, 'ajaxUpdateAttachmentMeta'], 0);
    }

    public function ajaxGetAttachment()
    {
        list($options, $field) = $this->ajaxGetAttachmentFromGallery();

        $mediaForm = $this->renderAttachmentFromGallery($options, $field);
        echo $this->injectMediaMetaFields($mediaForm, $options, $field);

        wp_die();
    }

    public function ajaxGetAttachmentMeta()
    {
        list($options, $field) = $this->ajaxGetAttachmentFromGallery();

        if (! $field['sub_fields']) {
            wp_die();
        }

        include acf_gallery_meta()->path('assets/view/attachment-metas.php');

        wp_die();
    }

    public function ajaxUpdateAttachmentMeta()
    {
        if (! $id = absint(acf_maybe_get($_REQUEST, 'id', false))) wp_send_json_error();

        check_ajax_referer('update-post_' . $id, 'nonce');

        if (! current_user_can('edit_post', $id)) wp_send_json_error();
        if (get_post_type($id) != 'attachment') wp_send_json_error();

        acf_save_post($id);
    }

    protected function ajaxGetAttachmentFromGallery()
    {
        // pro/fields/class-acf-field-gallery.php:ajax_get_attachment()
        $options = acf_parse_args($_POST, [
            'post_id'    => 0,
            'attachment' => 0,
            'id'         => 0,
            'field_key'  => '',
            'nonce'      => '',
        ]);

        if (! wp_verify_nonce($options['nonce'], 'acf_nonce')) die();
        if (! $options['id']) die();
        if (! $field = acf_get_field($options['field_key'])) die();

        return [$options, $field];
    }

    protected function renderAttachmentFromGallery($options, $field)
    {
        ob_start();
        $this->galleryField->render_attachment($options['id'], $field);
        return ob_get_clean();
    }

    protected function injectMediaMetaFields($mediaForm, $options, $field)
    {
        $pointerToSplit = strrpos($mediaForm, '</tbody>');
        $beforePointer = substr($mediaForm, 0, $pointerToSplit);
        $afterPointer = substr($mediaForm, $pointerToSplit);

        foreach ($field['hidden_media_fields'] as $name) {
            $beforePointer = str_replace(
                "data-name=\"${name}\"",
                "data-name=\"${name}\" style=\"display: none\"",
                $beforePointer
            );
        }

        $mediaMetaFields = $this->renderMediaMetaFields($options, $field);

        return implode('', [$beforePointer, $mediaMetaFields, $afterPointer]);
    }

    protected function renderMediaMetaFields($options, $field, $layout = 'tr')
    {
        ob_start();

        foreach ($field['sub_fields'] as $subField) {
            acf_render_field_wrap(acf_parse_args([
                'value' => acf_get_value($options['id'], $subField)
            ], $subField), $layout);
        }

        return ob_get_clean();
    }
}
