<?php

namespace GummiIO\AcfGalleryMeta\AcfGallery;

class Field
{
    public function __construct()
    {
        add_action('acf/render_field_settings/type=gallery', [$this, 'appendRepeaterFields'], 20);

        add_filter('acf/load_field/type=gallery', [$this, 'loadField']);
        add_filter('acf/update_field/type=gallery', [$this, 'updateField']);
        add_filter('acf/format_value/type=gallery', [$this, 'formatValue'], 20, 3);
    }

    public function appendRepeaterFields($field)
    {
        include acf_gallery_meta()->path('assets/view/repeater-fields.php');

        acf_render_field_setting($field, array(
            'name'          => 'hidden_media_fields',
            'label'         => __('Hide default media fields', 'acf-gallery_meta'),
            'instructions'  => __('Default fields to hide when editing gallery media in the gallery window.', 'acf-gallery_meta'),
            'type'          => 'checkbox',
            'choices'       => [
                'title'       => __('Title', 'acf'),
                'caption'     => __('Caption', 'acf'),
                'alt'         => __('Alt Text', 'acf'),
                'description' => __('Description', 'acf'),
            ]
        ));
    }

    public function loadField($field)
    {
        if (! isset($field['sub_fields'])) {
            $field['sub_fields'] = acf_get_fields($field)? : [];
        }

        if (! isset($field['hidden_media_fields'])) {
            $field['hidden_media_fields'] = [];
        }

        return $field;
    }

    public function updateField($field)
    {
        unset($field['sub_fields']);

        return $field;
    }

    public function formatValue($value, $post_id, $field)
    {
        if (! $value) return $value;

        return array_map(function ($post) use ($field) {
            return $this->appendMediaMetaValues($post, $field);
        }, $value);
    }

    protected function appendMediaMetaValues($post, $field)
    {
        foreach ($field['sub_fields'] as $subField) {
            $post[$subField['name']] = get_field($subField['key'], $post['ID']);
        }

        return $post;
    }
}
