<?php

namespace Tests;

use Tests\AcfSupports;

class AjaxTestCase extends \WP_Ajax_UnitTestCase
{
    use AcfSupports;

    public function setUp()
    {
        parent::setUp();

        $this->acfClearLocals();
    }

    protected function createAttachment($name = 'image.jpg', $title = 'Image File')
    {
        return $this->factory->attachment->create_object($name, 0, [
            'post_title' => $title,
            'post_mime_type' => 'image/jpeg',
            'post_type' => 'attachment'
        ]);
    }
}
