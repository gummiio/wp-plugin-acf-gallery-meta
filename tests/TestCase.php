<?php

namespace Tests;

use Tests\AcfSupports;

class TestCase extends \WP_UnitTestCase
{
    use AcfSupports;

    public function setUp()
    {
        parent::setUp();

        $this->acfClearLocals();
    }

    protected function getOutput($callable, $args = [])
    {
        ob_start();
        call_user_func_array($callable, $args);
        return ob_get_clean();
    }

    protected function createAttachment($name = 'image.jpg', $title = 'Image File')
    {
        return $this->factory->attachment->create_object($name, 0, [
            'post_title' => $title,
            'post_mime_type' => 'image/jpeg',
            'post_type' => 'attachment'
        ]);
    }
}
