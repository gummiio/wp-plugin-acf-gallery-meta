<?php

namespace Test\Feature\AcfGallery;

use Tests\TestCase;

class FieldTest extends TestCase
{
    /** @test */
    public function it_will_append_sub_field_settings_and_hidden_media_fields_settings()
    {
        $output = $this->getOutput(function() {
            $field = $this->acfLocalField('gallery', 'gallery_field');
            do_action('acf/render_field_settings/type=gallery', $field);
        });

        $this->assertRegExp('/data-name="sub_fields"/', $output);
        $this->assertRegExp('/data-name="hidden_media_fields"/', $output);
    }

    /** @test */
    public function gallery_field_will_always_have_sub_fields_and_hidden_media_fields_keys()
    {
        $field = $this->acfLocalField('gallery', 'gallery_field');
        $field = acf_get_field($field['key']);

        $this->assertArrayHasKey('sub_fields', $field);
        $this->assertArrayHasKey('hidden_media_fields', $field);
    }

    /** @test */
    public function it_should_not_save_the_sub_fields_into_database()
    {
        $field = $this->acfLocalField('gallery', 'gallery_field');
        $field = _acf_get_field_by_key($field['key']);

        $dbField = $this->acfDbField('gallery', 'gallery_field');
        $dbField = _acf_get_field_by_id($dbField['ID']);

        $this->assertArrayNotHasKey('sub_fields', $field);
    }

    /** @test */
    public function it_will_append_custom_meta_value_to_media_array()
    {
        $field = $this->acfLocalField('gallery', 'gallery_field', [
            'sub_fields' => [
                $this->acfLocalField('text', 'text_1'),
                $this->acfLocalField('text', 'text_2'),
            ]
        ]);

        $post = $this->factory->post->create();
        $attachment = $this->createAttachment('image.jpg');

        update_field('gallery_field', [$attachment], $post);
        update_field('text_1', 'value 1', $attachment);
        update_field('text_2', 'value 2', $attachment);

        $values = get_field('gallery_field', $post);

        $this->assertNotEmpty($values);
        $this->assertEquals('value 1', $values[0]['text_1']);
        $this->assertEquals('value 2', $values[0]['text_2']);
        $this->assertEquals('value 1', get_field('text_1', $values[0]['ID']));
        $this->assertEquals('value 2', get_field('text_2', $values[0]['ID']));
    }
}
