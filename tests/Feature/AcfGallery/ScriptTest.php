<?php

namespace Test\Feature\AcfGallery;

use Tests\TestCase;

class ScriptTest extends TestCase
{
    /** @test */
    public function it_will_load_input_script_on_acf_pages()
    {
        $this->assertTrue(!! has_action(
            'acf/input/admin_enqueue_scripts',
            [acf_gallery_meta('galleryScript'), 'enqueyeInputScripts']
        ));
    }
}
