<?php

namespace Test\Feature\AcfGallery;

use Tests\AjaxTestCase;
use \WPAjaxDieContinueException;

class AjaxTest extends AjaxTestCase
{
    public function setUp()
    {
        parent::setUp();

        $_POST = [];

        $this->field = $this->acfLocalField('gallery', 'gallery_field', [
            'sub_fields' => [
                $this->text_1 = $this->acfLocalField('text', 'text_1'),
                $this->text_2 = $this->acfLocalField('text', 'text_2'),
            ],
            'hidden_media_fields' => ['description']
        ]);

        $this->post = $this->factory->post->create();
        $this->attachment = $this->createAttachment('image.jpg');

        update_field('gallery_field', [$this->attachment], $this->post);
        update_field('text_1', 'value 1', $this->attachment);
        update_field('text_2', 'value 2', $this->attachment);
    }

    /** @test */
    public function it_should_overwrite_the_default_get_attachment_returns()
    {
        $gallery = acf_get_field_type('gallery');
        $ajax = acf_gallery_meta('galleryAjax');

        $this->assertSame(false, has_action(
            'wp_ajax_acf/fields/gallery/get_attachment',
            [$gallery, 'ajax_get_attachment']
        ));

        $this->assertSame(false, has_action(
            'wp_ajax_nopriv_acf/fields/gallery/get_attachment',
            [$gallery, 'ajax_get_attachment']
        ));

        $this->assertNotSame(false, has_action(
            'wp_ajax_acf/fields/gallery/get_attachment',
            [$ajax, 'ajaxGetAttachment']
        ));

        $this->assertNotSame(false, has_action(
            'wp_ajax_nopriv_acf/fields/gallery/get_attachment',
            [$ajax, 'ajaxGetAttachment']
        ));
    }

    /** @test */
    public function it_will_setup_get_attachment_ajax_hook()
    {
        $ajax = acf_gallery_meta('galleryAjax');

        $this->assertNotSame(false, has_action(
            'wp_ajax_acf/fields/gallery/get_attachment_meta',
            [$ajax, 'ajaxGetAttachmentMeta']
        ));

        $this->assertNotSame(false, has_action(
            'wp_ajax_nopriv_acf/fields/gallery/get_attachment_meta',
            [$ajax, 'ajaxGetAttachmentMeta']
        ));
    }

    /** @test */
    public function it_will_add_action_to_handle_attachment_save()
    {
        $ajax = acf_gallery_meta('galleryAjax');

        $this->assertNotSame(false, has_action('wp_ajax_save-attachment-compat', [$ajax, 'ajaxUpdateAttachmentMeta']));
    }

    /** @test */
    public function it_will_return_meta_fields_in_ajax_response_on_get_attachment()
    {
        $this->setupGetAttachmentPost();

        $this->ajax('acf/fields/gallery/get_attachment');

        $this->assertRegExp('/data-name="text_1"/', $this->_last_response);
        $this->assertRegExp('/data-name="text_2"/', $this->_last_response);
    }

    /** @test */
    public function it_will_add_display_non_to_hidden_media_fields()
    {
        $this->setupGetAttachmentPost();

        $this->ajax('acf/fields/gallery/get_attachment');

        $this->assertRegExp('/data-name="description" style="display:\s?none"/', $this->_last_response);
    }

    /** @test */
    public function it_will_return_meta_fields_in_ajax_response_on_get_attachment_meta()
    {
        $this->setupGetAttachmentPost([
            'action' => 'acf/fields/gallery/get_attachment_meta'
        ]);

        $this->ajax('acf/fields/gallery/get_attachment_meta');

        $this->assertRegExp('/data-name="text_1"/', $this->_last_response);
        $this->assertRegExp('/data-name="text_2"/', $this->_last_response);
    }

    /** @test */
    public function it_will_add_css_to_hidden_media_fields_fields_on_get_attachment_meta()
    {
        $this->setupGetAttachmentPost([
            'action' => 'acf/fields/gallery/get_attachment_meta'
        ]);

        $this->ajax('acf/fields/gallery/get_attachment_meta');

        $this->assertRegExp(
            '/\[data-setting="description"\] \{ display: none !important; \}/',
            $this->_last_response
        );
    }

    /** @test */
    public function it_will_update_field_value_for_media()
    {
        $admin = $this->factory->user->create(['role' => 'administrator']);
        wp_set_current_user($admin);

        $this->setupGetAttachmentPost([
            'action' => 'acf/fields/gallery/update_attachment',
            "attachments" => [$this->attachment => []],
            'acf' => [$this->text_1['key'] => 'updated']
        ]);

        $this->assertEquals('value 1', get_field('text_1', $this->attachment));

        $this->ajax('acf/fields/gallery/update_attachment');

        $this->assertEquals('updated', get_field('text_1', $this->attachment));
    }

    /** @test */
    public function it_will_update_field_value_via_edit_popup()
    {
        $admin = $this->factory->user->create(['role' => 'administrator']);
        wp_set_current_user($admin);

        $this->setupGetAttachmentPost([
            'nonce' => wp_create_nonce("update-post_{$this->attachment}"),
            'action' => 'save-attachment-compat',
            'acf' => [$this->text_1['key'] => 'updated']
        ]);

        $this->assertEquals('value 1', get_field('text_1', $this->attachment));

        $this->ajax('save-attachment-compat');

        $this->assertEquals('updated', get_field('text_1', $this->attachment));
    }

    protected function ajax($action)
    {
        try {
            $this->_handleAjax($action);
        } catch (WPAjaxDieContinueException $e) {
            unset($e);
        }
    }

    protected function setupGetAttachmentPost($post = [])
    {
        $_POST = wp_parse_args($post, [
            'nonce' => wp_create_nonce('acf_nonce'),
            'post_id' => $this->post,
            'action' => 'acf/fields/gallery/get_attachment',
            'field_key' => $this->field['key'],
            'id' => $this->attachment
        ]);
    }
}
