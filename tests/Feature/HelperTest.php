<?php

namespace Tests\Feature;

use GummiIO\AcfGalleryMeta\GalleryMeta;
use Tests\TestCase;

class HelperTest extends TestCase
{
    /** @test */
    public function it_can_retreive_acf_gallery_meta_instance()
    {
        global $acfGalleryMeta;

        $this->assertInstanceOf(GalleryMeta::class, acf_gallery_meta());
        $this->assertSame($acfGalleryMeta, acf_gallery_meta());
    }
}
